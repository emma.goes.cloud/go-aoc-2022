package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type position struct {
	x float64
	y float64
}

type rope struct {
	head position
	tail position
}

func (r *rope) getDistance() float64 {
	xd := math.Abs(r.head.x - r.tail.x)
	yd := math.Abs(r.head.y - r.tail.y)
	switch xd {
	case 0:
		switch yd {
		case 0:
			return 0
		default:
			return yd
		}
	default:
		switch yd {
		case 0:
			return xd
		default:
			return math.Sqrt(xd*xd + yd*yd)
		}
	}
}

func (r *rope) move(d string) {
	distance := r.getDistance()
	switch distance {
	case 0:
		switch d {
		case "R":
			r.head.x = r.head.x + 1
			return
		case "L":
			r.head.x = r.head.x - 1
			return
		case "U":
			r.head.y = r.head.y + 1
			return
		case "D":
			r.head.y = r.head.y - 1
			return
		}
	case 1:
		switch d {
		case "R":
			r.head.x = r.head.x + 1
			ndistance := r.getDistance()
			if ndistance == 2 {
				r.tail.x = r.tail.x + 1
			}
			return
		case "L":
			r.head.x = r.head.x - 1
			ndistance := r.getDistance()
			if ndistance == 2 {
				r.tail.x = r.tail.x - 1
			}
			return
		case "U":
			r.head.y = r.head.y + 1
			ndistance := r.getDistance()
			if ndistance == 2 {
				r.tail.y = r.tail.y + 1
			}
			return
		case "D":
			r.head.y = r.head.y - 1
			ndistance := r.getDistance()
			if ndistance == 2 {
				r.tail.y = r.tail.y - 1
			}
			return

		}
	case math.Sqrt(2):
		switch d {
		case "R":
			tmp := r.head
			r.head.x = r.head.x + 1
			ndistance := r.getDistance()
			if ndistance == math.Sqrt(5) {
				r.tail = tmp
			}
			return
		case "L":
			tmp := r.head
			r.head.x = r.head.x - 1
			ndistance := r.getDistance()
			if ndistance == math.Sqrt(5) {
				r.tail = tmp
			}
			return

		case "U":
			tmp := r.head
			r.head.y = r.head.y + 1
			ndistance := r.getDistance()
			if ndistance == math.Sqrt(5) {
				r.tail = tmp
			}
			return

		case "D":
			tmp := r.head
			r.head.y = r.head.y - 1
			ndistance := r.getDistance()
			if ndistance == math.Sqrt(5) {
				r.tail = tmp
			}
			return
		}
	}
}

func record(visited *[]position, p position) {
	for _, v := range *visited {
		if v.x == p.x && v.y == p.y {
			return
		}
	}
	*visited = append(*visited, p)
}

func main() {
	data, err := os.ReadFile("data/day09.txt")
	if err != nil {
		log.Fatal(err)
	}
	total, err := totalPosition(string(data))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(total)

}

func totalPosition(data string) (int, error) {
	input := strings.TrimSpace(data)
	motions := strings.Split(input, "\n")
	r := rope{
		head: position{
			x: 0,
			y: 0,
		},
		tail: position{
			x: 0,
			y: 0,
		},
	}

	visited := &[]position{
		{
			x: 0,
			y: 0,
		},
	}
	for _, motion := range motions {
		m := strings.Split(motion, " ")
		direction := m[0]
		step := m[1]
		s, err := strconv.Atoi(step)
		if err != nil {
			return 0, err
		}
		for i := 1; i <= s; i++ {
			r.move(direction)
			record(visited, r.tail)
		}
	}
	return len(*visited), nil
}
