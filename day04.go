package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func mainDay04() {
	data, err := os.ReadFile("data/day04.txt")
	if err != nil {
		log.Fatal(err)
	}

	count, err := countFullContain(string(data))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(count)

	countlap, err := countOverlap(string(data))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(countlap)

}

func convertInt(elf string) (int, int, error) {
	sectionRange := strings.Split(elf, "-")
	min, err := strconv.Atoi(sectionRange[0])
	if err != nil {
		return 0, 0, err
	}
	max, err := strconv.Atoi(sectionRange[1])
	if err != nil {
		return 0, 0, err
	}

	return min, max, nil

}

func countFullContain(input string) (int, error) {
	pairElves := strings.TrimSpace(input)
	pairs := strings.Split(pairElves, "\n")
	var count = 0
	for _, pair := range pairs {
		elves := strings.Split(pair, ",")

		firstElf := elves[0]
		secondElf := elves[1]
		firstMin, firstMax, err := convertInt(firstElf)
		if err != nil {
			return 0, err
		}

		secondMin, secondMax, err := convertInt(secondElf)
		if err != nil {
			return 0, err
		}

		var fullyContain = 0
		if firstMin <= secondMin && firstMax >= secondMax {
			fullyContain = 1
		} else if firstMin >= secondMin && firstMax <= secondMax {
			fullyContain = 1
		} else {
			fullyContain = 0
		}
		count = count + fullyContain
	}

	return count, nil

}

func countOverlap(input string) (int, error) {

	pairElves := strings.TrimSpace(input)
	pairs := strings.Split(pairElves, "\n")
	var count = 0
	for _, pair := range pairs {
		elves := strings.Split(pair, ",")

		firstElf := elves[0]
		secondElf := elves[1]
		firstMin, firstMax, err := convertInt(firstElf)
		if err != nil {
			return 0, err
		}

		secondMin, secondMax, err := convertInt(secondElf)
		if err != nil {
			return 0, err
		}

		var overlap = 0
		if firstMax < secondMin || secondMax < firstMin {
			overlap = 0
		} else {
			overlap = 1
		}
		count = count + overlap
	}

	return count, nil

}
