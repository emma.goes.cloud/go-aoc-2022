package main

import (
	"testing"

	"github.com/lithammer/dedent"
)

func TestTotalScore(t *testing.T) {
	testInput := string(`
                A Y
                B X
                C Z
	`)

	input := dedent.Dedent(testInput)
	want := 15
	t.Run(input, func(t *testing.T) {
		totalScor := totalScore(input)
		if totalScor != want {
			t.Errorf("got %d, want %d", totalScor, want)
		}
	})

}

func TestTotalScoreByEnd(t *testing.T) {
	testInput := string(`
                A Y
                B X
                C Z
	`)

	input := dedent.Dedent(testInput)
	want := 12
	t.Run(input, func(t *testing.T) {
		totalScor := totalScoreByEnd(input)
		if totalScor != want {
			t.Errorf("got %d, want %d", totalScor, want)
		}
	})

}
