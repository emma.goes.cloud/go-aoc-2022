package main

import (
	"log"
	"os"
	"testing"
)

func TestFindTree(t *testing.T) {
	testInput, err := os.ReadFile("data/day08_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 21

	t.Run(input, func(t *testing.T) {
		got, err := findTree(input)
		if err != nil {
			t.Error(err)
		}

		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}

func TestFindTreeDistance(t *testing.T) {
	testInput, err := os.ReadFile("data/day08_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 8

	t.Run(input, func(t *testing.T) {
		got, err := findTreeDistance(input)
		if err != nil {
			t.Error(err)
		}

		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}
