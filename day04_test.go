package main

import (
	"testing"

	"github.com/lithammer/dedent"
)

func TestCountFullContain(t *testing.T) {
	testInput := string(`
                2-4,6-8
                2-3,4-5
                5-7,7-9
                2-8,3-7
                6-6,4-6
                2-6,4-8
	`)

	input := dedent.Dedent(testInput)
	want := 2

	t.Run(input, func(t *testing.T) {
		count, err := countFullContain(input)
		if err != nil {
			t.Error(err)
		}

		if count != want {
			t.Errorf("got %d, want %d", count, want)
		}
	})

}

func TestOverlap(t *testing.T) {
	testInput := string(`
                2-4,6-8
                2-3,4-5
                5-7,7-9
                2-8,3-7
                6-6,4-6
                2-6,4-8
	`)

	input := dedent.Dedent(testInput)
	want := 4

	t.Run(input, func(t *testing.T) {
		count, err := countOverlap(input)
		if err != nil {
			t.Error(err)
		}

		if count != want {
			t.Errorf("got %d, want %d", count, want)
		}
	})

}
