package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"unicode"
)

func mainDay03() {
	data, err := os.ReadFile("data/day03.txt")
	if err != nil {
		log.Fatal(err)
	}
	totalPriority := sumPriority(string(data))
	fmt.Println(totalPriority)
	totalThreePriority := sumThreePriority(string(data))
	fmt.Println(totalThreePriority)
}

func sumPriority(input string) int {
	contents := strings.TrimSpace(input)
	rucksacks := strings.Split(contents, "\n")

	var totalPriority = 0
	for _, rucksack := range rucksacks {
		half := len(rucksack)/2 - 1

		for i := half + 1; i <= len(rucksack)-1; i++ {
			item := string(rucksack[i])

			isExit := strings.Contains(rucksack[:half+1], item)
			if isExit == true {
				var priorities = 0
				r := rune(rucksack[i])
				isUpper := unicode.IsUpper(r)
				if isUpper == true {
					offset := 38
					priorities = int(r) - offset
				} else {
					offset := 96
					priorities = int(r) - offset
				}
				totalPriority = totalPriority + priorities

				break
			}
		}

	}
	return totalPriority
}

func sumThreePriority(input string) int {
	contents := strings.TrimSpace(input)
	rucksacks := strings.Split(contents, "\n")

	var totalThreePriority = 0

	for i := 0; i <= len(rucksacks)-1; i = i + 3 {

		rucksack1 := rucksacks[i]
		rucksack2 := rucksacks[i+1]
		rucksack3 := rucksacks[i+2]

		for j := 0; j <= len(rucksack1)-1; j++ {

			item := string(rucksack1[j])
			isExit2 := strings.Contains(rucksack2, item)
			isExit3 := strings.Contains(rucksack3, item)

			if isExit2 == true && isExit3 == true {

				chr := rune(rucksack1[j])
				isUpper := unicode.IsUpper(chr)
				var priorities = 0
				if isUpper == true {
					offset := 38
					priorities = int(chr) - offset

				} else {
					offset := 96
					priorities = int(chr) - offset

				}
				totalThreePriority = totalThreePriority + priorities

				break
			}
		}
	}
	return totalThreePriority
}
