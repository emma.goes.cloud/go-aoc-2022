package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func mainDay06() {
	data, err := os.ReadFile("data/day06.txt")
	if err != nil {
		log.Fatal(err)
	}

	datastream := string(data)
	position := firstMarkerPosition(datastream)
	fmt.Println(position)

	mPosition := firstMeassPosition(datastream)
	fmt.Println(mPosition)

}

func firstMarkerPosition(datastream string) int {
	var position = 0
	for i := 0; i <= len(datastream)-1 && (i+4) <= len(datastream)-1; i++ {
		fourChr := datastream[i : i+4]
		var isDiff []bool
		for _, chr := range fourChr {
			count := strings.Count(fourChr, string(chr))
			if count == 1 {
				isDiff = append(isDiff, true)
			}
			//		fmt.Println(j, string(chr))
		}

		if len(isDiff) == 4 {
			position = i + 4
			//		fmt.Println(i, i+4, string(datastream[i]))
			break
		}

	}

	return position
}

func firstMeassPosition(datastream string) int {
	var position = 0
	for i := 0; i <= len(datastream)-1 && (i+14) <= len(datastream)-1; i++ {
		fourChr := datastream[i : i+14]
		var isDiff []bool
		for _, chr := range fourChr {
			count := strings.Count(fourChr, string(chr))
			if count == 1 {
				isDiff = append(isDiff, true)
			}
			//		fmt.Println(j, string(chr))
		}

		if len(isDiff) == 14 {
			position = i + 14
			//		fmt.Println(i, i+4, string(datastream[i]))
			break
		}

	}

	return position
}
