package main

import (
	"log"
	"os"
	"testing"
)

func TestFirstMarkerPosition(t *testing.T) {
	testInput, err := os.ReadFile("data/day06_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 7

	t.Run(input, func(t *testing.T) {
		got := firstMarkerPosition(input)
		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}

func TestFirstMeassPosition(t *testing.T) {
	testInput, err := os.ReadFile("data/day06_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 19

	t.Run(input, func(t *testing.T) {
		got := firstMeassPosition(input)
		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}
