package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type tree struct {
	height int
	id     int // postion
}

type nodeTable struct {
	up    *nodeTable
	down  *nodeTable
	left  *nodeTable
	right *nodeTable
	value tree
}

type table struct {
	root *nodeTable
}

func (t *table) insert(preId int, value tree, column int) *table {
	if t.root == nil {
		t.root = &nodeTable{
			up:    nil,
			down:  nil,
			left:  nil,
			right: nil,
			value: value,
		}
	} else {
		// search the pre nodeTable
		pre := t.root.search(preId)
		if pre == nil {
			fmt.Println("not found pre nodeTable")
		} else {
			// insert nodeTable
			// fmt.Println("found the nodeTable", pre.value.id, pre.value.height)
			pre.insert(value, column)
		}

	}
	return t
}

func (n *nodeTable) search(id int) *nodeTable {
	if n == nil {
		return nil
	}
	if n.value.id == id {
		return n
	}
	found := n.rightSearch(id)
	if found.value.id != id {
		found = found.down.leftSearch(id)
	}
	if found.value.id != id {
		found = found.down.search(id)
	}
	return found
}

func (n *nodeTable) rightSearch(id int) *nodeTable {
	if n == nil {
		return nil
	}
	if n.value.id == id {
		return n
	}
	found := n.right.rightSearch(id)
	if found == nil {
		// if not found , return down
		//	fmt.Println("return the rightest nodeTable", n.value.id, n.value.height, id)
		found = n
	}

	return found
}

func (n *nodeTable) leftSearch(id int) *nodeTable {

	if n == nil {
		return nil
	}
	if n.value.id == id {
		return n
	}
	found := n.left.leftSearch(id)
	if found == nil {
		// if not found , return down
		// fmt.Println("return the leftest nodeTable", n.value.id, n.value.height, id)
		found = n
	}
	return found

}

func (n *nodeTable) insert(value tree, column int) {
	if n.value.id%column == 0 {
		// newline
		if n.down == nil {
			n.down = &nodeTable{
				up:    n,
				down:  nil,
				left:  nil,
				right: nil,
				value: value,
			}
			// fmt.Println("Insert down tree", value.id, value.height)
			return
		} else {

			fmt.Println("the down nodeTable has existed !")
		}
	}

	if n.up == nil {
		// first line row
		if n.right == nil {
			n.right = &nodeTable{
				up:    nil,
				down:  nil,
				left:  n,
				value: value,
			}
			// fmt.Println("Insert top tree", value.id, value.height)
		} else {
			fmt.Println("the right nodeTable has existed !", n.right.value.id, n.right.value.height)
		}
	} else {
		if n.up.left != nil {
			if n.left == nil {
				newt := &nodeTable{
					up:    n.up.left,
					down:  nil,
					right: n,
					left:  nil,
					value: value,
				}
				n.left = newt
				n.up.left.down = newt
				//	fmt.Println("Insert left tree", value.id, value.height)
			}

		}

		if n.up.right != nil {
			if n.right == nil {
				newt := &nodeTable{
					up:    n.up.right,
					down:  nil,
					left:  n,
					right: nil,
					value: value,
				}
				n.right = newt
				n.up.right.down = newt
				// fmt.Println("Insert right tree", value.id, value.height)
			}
		}
	}
}

func (n *nodeTable) findTall() bool {
	if n == nil {
		return false
	}
	isUpTall := n.upFindTall(n.value.height)
	isDownTall := n.downFindTall(n.value.height)
	isLeftTall := n.leftFindTall(n.value.height)
	isRightTall := n.rightFindTall(n.value.height)

	tall := true
	if isUpTall == false && isDownTall == false && isLeftTall == false && isRightTall == false {
		tall = false
	}

	return tall

}

func (n *nodeTable) upFindTall(height int) bool {
	if n == nil {
		return true
	}
	if n.up != nil {
		if height > n.up.value.height {
			isTall := n.up.upFindTall(height)
			return isTall
		} else {
			return false
		}
	} else {
		return true
	}
}

func (n *nodeTable) downFindTall(height int) bool {
	if n == nil {
		return true
	}
	if n.down != nil {
		if height > n.down.value.height {
			isTall := n.down.downFindTall(height)
			return isTall
		} else {
			return false
		}
	} else {
		return true
	}
}

func (n *nodeTable) leftFindTall(height int) bool {
	if n == nil {
		return true
	}
	if n.left != nil {
		if height > n.left.value.height {
			isTall := n.left.leftFindTall(height)
			return isTall
		} else {
			return false
		}
	} else {
		return true
	}
}

func (n *nodeTable) rightFindTall(height int) bool {
	if n == nil {
		return true
	}
	if n.right != nil {
		if height > n.right.value.height {
			isTall := n.right.rightFindTall(height)
			return isTall
		} else {
			return false
		}
	} else {
		return true
	}
}

func (n *nodeTable) traverse() int {
	if n == nil {
		return 0
	}
	fmt.Println("print all Tree", n.value.id, n.value.height)
	isTall := n.findTall()
	y := 1
	if isTall == false {
		y = 0
	}

	k, count1 := n.right.rightTraverse()
	count1 = count1 + y

	h, count2 := k.down.leftTraverse()

	count2 = count1 + count2
	if h != nil && h.down != nil {

		count2 = count2 + h.down.traverse()
	}
	return count2
}

func (n *nodeTable) rightTraverse() (*nodeTable, int) {
	if n == nil {
		return nil, 0
	}
	// fmt.Println("print all right Tree", n.value.id, n.value.height)

	isTall := n.findTall()
	y := 1
	if isTall == false {
		y = 0
	}

	m, c := n.right.rightTraverse()

	c = c + y

	if m == nil {
		m = n
	}

	return m, c
}

func (n *nodeTable) leftTraverse() (*nodeTable, int) {
	if n == nil {
		return nil, 0
	}
	// fmt.Println("print all left Tree", n.value.id, n.value.height)
	isTall := n.findTall()
	y := 1
	if isTall == false {
		y = 0
	}

	m, c := n.left.leftTraverse()
	c = c + y

	if m == nil {
		m = n
	}
	return m, c
}

func (n *nodeTable) upDistance(height int) int {
	if n == nil {
		return 0
	}
	towTree := 1

	if height <= n.value.height {

		//	fmt.Println("up", n.value.id, height)
		return towTree
	}

	d := n.up.upDistance(height)
	d = d + towTree
	return d
}

func (n *nodeTable) downDistance(height int) int {
	if n == nil {
		return 0
	}
	towTree := 1

	if height <= n.value.height {
		//	fmt.Println("down", n.value.id, height)
		return towTree
	}

	d := n.down.downDistance(height)
	d = d + towTree
	return d
}

func (n *nodeTable) leftDistance(height int) int {
	if n == nil {
		return 0
	}
	towTree := 1

	if height <= n.value.height {
		//	fmt.Println("left", n.value.id, height)
		return towTree
	}

	d := n.left.leftDistance(height)
	d = d + towTree
	return d
}

func (n *nodeTable) rightDistance(height int) int {
	if n == nil {
		return 0
	}
	towTree := 1

	if height <= n.value.height {
		//	fmt.Println("right", n.value.id, height)
		return towTree
	}

	d := n.right.rightDistance(height)
	d = d + towTree
	return d
}

func (n *nodeTable) allDistance() int {
	up := n.up.upDistance(n.value.height)
	down := n.down.downDistance(n.value.height)
	left := n.left.leftDistance(n.value.height)
	right := n.right.rightDistance(n.value.height)
	scenicScore := up * down * left * right
	return scenicScore

}

func (n *nodeTable) rightTr() (*nodeTable, int) {
	if n == nil {
		return nil, 0
	}
	scenicScore := n.allDistance()
	r, max := n.right.rightTr()

	if r == nil {
		r = n
	}
	if scenicScore > max {
		max = scenicScore
	}
	return r, max
}

func (n *nodeTable) leftTr() (*nodeTable, int) {
	if n == nil {
		return nil, 0
	}
	scenicScore := n.allDistance()
	l, max := n.right.rightTr()

	if l == nil {
		l = n
	}
	if scenicScore > max {
		max = scenicScore
	}
	return l, max
}

func (n *nodeTable) Tr() int {
	if n == nil {
		return 0
	}
	scenicScore := n.allDistance()
	r, maxR := n.right.rightTr()
	if scenicScore > maxR {
		maxR = scenicScore
	}

	_, maxL := r.down.leftTr()
	if maxL > maxR {
		maxR = maxL
	}
	max := n.down.Tr()
	if maxR > max {
		max = maxR
	}
	return max
}

func mainDay08() {
	data, err := os.ReadFile("data/day08.txt")
	if err != nil {
		log.Fatal(err)
	}
	allTall, err := findTree(string(data))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("taltal Tree", allTall)
	maxScenicScore, err := findTreeDistance(string(data))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("maxScenicScore:", maxScenicScore)
}

func findTree(data string) (int, error) {
	input := strings.TrimSpace(data)
	rows := strings.Split(input, "\n")

	count := 0
	var tb = &table{
		root: nil,
	}

	for i, row := range rows {
		if i%2 == 0 {
			// read from left
			for _, tr := range row {
				trIn, err := strconv.Atoi(string(tr))
				if err != nil {
					return 0, err
				}

				count++
				value := tree{
					id:     count,
					height: trIn,
				}
				// fmt.Println("tree height:", trIn, "tree id:", count, " the line", i, "column:", len(row))
				tb = tb.insert(count-1, value, len(row))
			}

		} else {
			// read from right

			for j := len(row) - 1; j >= 0; j-- {
				trIn, err := strconv.Atoi(string(row[j]))
				if err != nil {
					return 0, err
				}

				count++
				value := tree{
					id:     count,
					height: trIn,
				}
				// fmt.Println("tree height:", trIn, "treeId:", count, " the line", i, "column:", len(row))
				tb = tb.insert(count-1, value, len(row))

			}
		}

	}
	allTall := tb.root.traverse()
	return allTall, nil
}

func findTreeDistance(data string) (int, error) {
	input := strings.TrimSpace(data)
	rows := strings.Split(input, "\n")

	count := 0
	var tb = &table{
		root: nil,
	}

	for i, row := range rows {
		if i%2 == 0 {
			// read from left
			for _, tr := range row {
				trIn, err := strconv.Atoi(string(tr))
				if err != nil {
					return 0, err
				}

				count++
				value := tree{
					id:     count,
					height: trIn,
				}
				// fmt.Println("tree height:", trIn, "tree id:", count, " the line", i, "column:", len(row))
				tb = tb.insert(count-1, value, len(row))
			}

		} else {
			// read from right

			for j := len(row) - 1; j >= 0; j-- {
				trIn, err := strconv.Atoi(string(row[j]))
				if err != nil {
					return 0, err
				}

				count++
				value := tree{
					id:     count,
					height: trIn,
				}
				//fmt.Println("tree height:", trIn, "treeId:", count, " the line", i, "column:", len(row))
				tb = tb.insert(count-1, value, len(row))

			}
		}

	}
	maxScenicScore := tb.root.Tr()
	return maxScenicScore, nil
}
