package main

import (
	"testing"

	"github.com/lithammer/dedent"
)

func TestSumPriority(t *testing.T) {
	testInput := string(`
                 vJrwpWtwJgWrhcsFMMfFFhFp
                 jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
                 PmmdzqPrVvPwwTWBwg
                 wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
                 ttgJtRGJQctTZtZT
                 CrZsJsPPZsGzwwsLwLmpwMDw
	`)

	input := dedent.Dedent(testInput)

	want := 157

	t.Run(input, func(t *testing.T) {
		totalPriority := sumPriority(input)
		if totalPriority != want {
			t.Errorf("got %d, want %d", totalPriority, want)
		}
	})
}

func TestSumThreePriority(t *testing.T) {
	testInput := string(`
                 vJrwpWtwJgWrhcsFMMfFFhFp
                 jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
                 PmmdzqPrVvPwwTWBwg
                 wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
                 ttgJtRGJQctTZtZT
                 CrZsJsPPZsGzwwsLwLmpwMDw
	`)

	input := dedent.Dedent(testInput)

	want := 70

	t.Run(input, func(t *testing.T) {
		totalPriority := sumThreePriority(input)
		if totalPriority != want {
			t.Errorf("got %d, want %d", totalPriority, want)
		}
	})
}
