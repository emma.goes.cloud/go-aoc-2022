package main

import (
	"os"
	"testing"
)

func TestFindMessage(t *testing.T) {
	data, err := os.ReadFile("data/day05_test.txt")
	if err != nil {
		t.Error(err)
	}

	input := string(data)
	want := "CMZ"
	t.Run(input, func(t *testing.T) {
		message, err := findMessage(input)
		if err != nil {
			t.Error(err)
		}

		if message != want {
			t.Errorf("got %s , want %s", message, want)
		}

	})
}

func TestFindMessageBySameOrder(t *testing.T) {
	data, err := os.ReadFile("data/day05_test.txt")
	if err != nil {
		t.Error(err)
	}

	input := string(data)
	want := "MCD"
	t.Run(input, func(t *testing.T) {
		message, err := findMessageBySameOrder(input)
		if err != nil {
			t.Error(err)
		}

		if message != want {
			t.Errorf("got %s , want %s", message, want)
		}

	})
}
