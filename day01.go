package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func mainDay01() {
	data, err := os.ReadFile("data/day01.txt")
	if err != nil {
		log.Fatal(err)
	}

	maxCalorie, err := MaxCalorie(string(data))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(maxCalorie)

	topThreeCal, err := TopThreeCalorie(string(data))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(topThreeCal)
}

func MaxCalorie(input string) (int, error) {
	var calories []int

	strim := strings.TrimSpace(input)
	slice := strings.Split(strim, "\n\n")
	for _, s := range slice {
		var calorie int = 0
		calStr := strings.Split(s, "\n")
		for _, cal := range calStr {
			i, err := strconv.Atoi(cal)
			if err != nil {
				return 0, err
			}
			calorie = i + calorie
		}
		calories = append(calories, calorie)
	}

	sort.Slice(calories, func(i, j int) bool {
		return calories[i] > calories[j]
	})
	maxCal := calories[0]
	return maxCal, nil
}

func TopThreeCalorie(input string) (int, error) {
	var calories []int

	strim := strings.TrimSpace(input)
	slice := strings.Split(strim, "\n\n")
	for _, s := range slice {
		var calorie int = 0
		calStr := strings.Split(s, "\n")
		for _, cal := range calStr {
			i, err := strconv.Atoi(cal)
			if err != nil {
				return 0, err
			}
			calorie = i + calorie
		}
		calories = append(calories, calorie)
	}

	sort.Slice(calories, func(i, j int) bool {
		return calories[i] > calories[j]
	})
	topThreeCal := calories[0] + calories[1] + calories[2]
	return topThreeCal, nil
}
