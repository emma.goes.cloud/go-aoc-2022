package main

import (
	"log"
	"os"
	"testing"
)

func TestAllFileSize(t *testing.T) {
	testInput, err := os.ReadFile("data/day07_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 95437

	t.Run(input, func(t *testing.T) {
		got, err := allFileSize(input)
		if err != nil {
			t.Error(err)
		}

		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}

func TestAtLeastSize(t *testing.T) {
	testInput, err := os.ReadFile("data/day07_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 24933642

	t.Run(input, func(t *testing.T) {
		got, err := atLeastSize(input)
		if err != nil {
			t.Error(err)
		}

		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}
