package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type (
	Stack struct {
		top    *node
		length int
	}

	node struct {
		value interface{}
		prev  *node
	}
)

func New() *Stack {
	return &Stack{nil, 0}
}

func (this *Stack) Len() int {
	return this.length
}

func (this *Stack) Peek() interface{} {
	if this.length == 0 {
		return nil
	}

	return this.top.value

}

func (this *Stack) Pop() interface{} {
	if this.length == 0 {
		return nil
	}

	n := this.top
	this.top = n.prev
	this.length--

	return n.value
}

func (this *Stack) Push(value interface{}) {
	n := &node{
		value: value,
		prev:  this.top,
	}

	this.top = n

	this.length++
}

func mainDay05() {
	data, err := os.ReadFile("data/day05.txt")
	if err != nil {
		log.Fatal(err)
	}

	messages, err := findMessage(string(data))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(messages)

	messagesBySameOrder, err := findMessageBySameOrder(string(data))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(messagesBySameOrder)

}

func findMessage(data string) (string, error) {
	inputPre := strings.TrimSuffix(data, " ")
	input := strings.TrimSuffix(inputPre, "\n")
	drawing := strings.Split(input, "\n\n")

	crates := drawing[0]
	procedure := drawing[1]

	// initial crates stacks
	stackCrts := strings.Split(crates, "\n")

	stackNumSerialPre := stackCrts[len(stackCrts)-1]
	stackNumSerial := strings.TrimSpace(stackNumSerialPre)
	stackNum := string(stackNumSerial[len(stackNumSerial)-1])
	stackSum, err := strconv.Atoi(stackNum)

	for err != nil {
		return "", err
	}

	var stackSlice = []*Stack{
		{
			top:    nil,
			length: 0,
		},
	}

	for i := 1; i <= stackSum; i++ {
		stack := New()
		stackSlice = append(stackSlice, stack)
	}

	for i := len(stackCrts) - 2; i >= 0; i-- {
		crts := stackCrts[i]
		//process 0 stacks
		if string(crts[1]) != " " {
			stackSlice[0].Push(string(crts[1]))
		}
		// process 1-8 stacks
		for j := 3; j <= len(crts)-1; j = j + 4 {
			if string(crts[j+2]) != " " {
				// get stackIndex
				k := j/4 + 1
				stackSlice[k].Push(string(crts[j+2]))

			}
		}

	}

	// rearrangement procedure
	steps := strings.Split(procedure, "\n")
	for _, step := range steps {
		pds := strings.Split(step, " ")

		move, err := strconv.Atoi(pds[1])
		if err != nil {
			return "", err
		}

		from, err := strconv.Atoi(pds[3])
		if err != nil {
			return "", err
		}

		to, err := strconv.Atoi(pds[5])
		if err != nil {
			return "", err
		}

		for m := move; m >= 1; m-- {

			topCrate := stackSlice[from-1].Peek()
			stackSlice[from-1].Pop()
			stackSlice[to-1].Push(topCrate)

		}
	}
	var messages = ""
	for _, message := range stackSlice {
		if message.Peek() != nil {
			messages = messages + message.Peek().(string)
		}
	}

	return messages, nil

}

func findMessageBySameOrder(data string) (string, error) {
	inputPre := strings.TrimSuffix(data, " ")
	input := strings.TrimSuffix(inputPre, "\n")
	drawing := strings.Split(input, "\n\n")

	crates := drawing[0]
	procedure := drawing[1]

	// initial crates stacks
	stackCrts := strings.Split(crates, "\n")

	stackNumSerialPre := stackCrts[len(stackCrts)-1]
	stackNumSerial := strings.TrimSpace(stackNumSerialPre)
	stackNum := string(stackNumSerial[len(stackNumSerial)-1])
	stackSum, err := strconv.Atoi(stackNum)

	for err != nil {
		return "", err
	}

	var stackSlice = []*Stack{
		{
			top:    nil,
			length: 0,
		},
	}

	for i := 1; i <= stackSum; i++ {
		stack := New()
		stackSlice = append(stackSlice, stack)
	}

	for i := len(stackCrts) - 2; i >= 0; i-- {
		crts := stackCrts[i]
		//process 0 stacks
		if string(crts[1]) != " " {
			stackSlice[0].Push(string(crts[1]))
		}
		// process 1-8 stacks
		for j := 3; j <= len(crts)-1; j = j + 4 {
			if string(crts[j+2]) != " " {
				// get stackIndex
				k := j/4 + 1
				stackSlice[k].Push(string(crts[j+2]))

			}
		}

	}

	// rearrangement procedure
	steps := strings.Split(procedure, "\n")
	for _, step := range steps {
		pds := strings.Split(step, " ")

		move, err := strconv.Atoi(pds[1])
		if err != nil {
			return "", err
		}

		from, err := strconv.Atoi(pds[3])
		if err != nil {
			return "", err
		}

		to, err := strconv.Atoi(pds[5])
		if err != nil {
			return "", err
		}

		tempStack := &Stack{
			top:    nil,
			length: 0,
		}

		for m := move; m >= 1; m-- {

			topCrate := stackSlice[from-1].Peek()
			tempStack.Push(topCrate)
			stackSlice[from-1].Pop()

		}

		for m := move; m >= 1; m-- {
			topCrate := tempStack.Peek()
			tempStack.Pop()
			stackSlice[to-1].Push(topCrate)
		}
	}
	var messages = ""
	for _, message := range stackSlice {
		if message.Peek() != nil {
			messages = messages + message.Peek().(string)
		}
	}

	return messages, nil

}
