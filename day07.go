package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type file struct {
	position int
	size     int
	path     string
	name     string
	isfile   bool
}

type BinaryNode struct {
	left  *BinaryNode
	right *BinaryNode
	data  file
}

type BinaryTree struct {
	root *BinaryNode
}

func (t *BinaryTree) insert(parentPath string, data file) *BinaryTree {
	if t.root == nil {
		t.root = &BinaryNode{
			data:  data,
			left:  nil,
			right: nil,
		}
		fmt.Println("root is ", parentPath)
	} else {
		// search parent in tree and insert data
		pre := t.root.search(parentPath)
		if pre != nil {
			pre.insert(data)

		} else {
			fmt.Println("parent don't exit in tree", parentPath)
		}
	}
	return t
}

func (n *BinaryNode) search(parentPath string) *BinaryNode {
	if n == nil {
		return nil
	} else {

		if n.data.path == parentPath {

			return n

		} else {
			foundNode := n.left.search(parentPath)
			if foundNode == nil {
				foundNode = n.right.search(parentPath)
			}
			return foundNode
		}
	}

}

func (n *BinaryNode) atMostSum() int {
	if n == nil {
		return 0
	}
	fmt.Println("size:", n.data.size, "path:", n.data.path, "isfile", n.data.isfile)
	m := n.data.size
	if n.data.size > 100000 || n.data.isfile == true {
		m = 0
	}
	return m + n.left.atMostSum() + n.right.atMostSum()
}

func (n *BinaryNode) atLeast(le int) int {
	if n == nil {
		MaxUint := ^uint(0)
		inf := int(MaxUint >> 1)

		return inf
	}

	m := n.data.size

	fmt.Println("size:", n.data.size, "path:", n.data.path, "isfile", n.data.isfile, "lesat size", le)

	if n.data.size < le || n.data.isfile == true {
		MaxUint := ^uint(0)
		m = int(MaxUint >> 1)
	}

	left := n.left.atLeast(le)
	if left < m {
		m = left
		fmt.Println("left is small:", m, "least size", le)
	}

	right := n.right.atLeast(le)
	if right < m {
		m = right
		fmt.Println("right is small:", m, "least size", le)
	}
	fmt.Println("return value", m, "node:", n.data.path)
	return m

}

func (n *BinaryNode) insert(data file) {
	if n == nil {
		return
	} else if data.position == 1 {
		// become left BinaryNode
		if n.left == nil {
			n.left = &BinaryNode{
				data:  data,
				left:  nil,
				right: nil,
			}
		} else {
			fmt.Println("find wrong Parent, Please refind parent", n.left.data.name, n.data.name, data.name)
			return
		}
	} else {
		if n.left.right == nil {
			n.left.right = &BinaryNode{
				data:  data,
				left:  nil,
				right: nil,
			}
		} else {
			n.left.right.rightInsert(data)
		}
	}
}

func (n *BinaryNode) rightInsert(data file) {
	if n == nil {
		return
	} else {
		if n.right == nil {
			n.right = &BinaryNode{
				data:  data,
				left:  nil,
				right: nil,
			}
		} else {
			n.right.rightInsert(data)
		}
	}

}

func addSize(t *BinaryTree, parentPath string, data file) {

	if parentPath == "/" {

		t.root.data.size = t.root.data.size + data.size
		fmt.Println("root Path", parentPath, t.root.data.size, data.size)
	} else {

		parents := strings.Split(parentPath, "/")

		for i := len(parents) - 1; i >= 0; i-- {

			count := strings.Count(parentPath, "/")
			if count <= 1 {
				pre := t.root.search(parentPath)

				pre.data.size = pre.data.size + data.size

				// add root sieze

				t.root.data.size = t.root.data.size + data.size

				break
			}

			pre := t.root.search(parentPath)

			pre.data.size = pre.data.size + data.size

			parentPath = cutPath(parentPath)

		}
	}
}

func cutPath(lastPath string) string {
	re := regexp.MustCompile(`/\w*\z`)
	trim := re.Find([]byte(lastPath))
	path := strings.TrimSuffix(lastPath, string(trim))
	return path
}

func mainDay07() {
	data, err := os.ReadFile("data/day07.txt")
	if err != nil {
		log.Fatal(err)
	}
	f, err := allFileSize(string(data))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(f)

	l, err := atLeastSize(string(data))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(l)

}

func allFileSize(fileData string) (int, error) {
	input := strings.TrimSpace(fileData)
	inputTrim := strings.TrimSpace(input)
	commands := strings.Split(inputTrim, "$")
	var b = &BinaryTree{
		root: nil,
	}
	pPath := ""
	currentPath := ""
	for _, command := range commands {
		isCd := strings.Contains(command, " cd ")
		if isCd == true {
			commandTrim := strings.TrimSpace(command)

			if strings.Compare(commandTrim[3:], "..") != 0 {

				if strings.Compare(commandTrim[3:], "/") == 0 {

					data := file{
						position: 0,
						size:     0,
						name:     "/",
						isfile:   false,
						path:     "/",
					}
					b = b.insert("/", data)
					pPath = "/"
				} else {
					// pick current parent
					cdCommand := strings.Split(commandTrim, " ")
					parent := cdCommand[1]
					currentPath = currentPath + "/" + parent
					pPath = currentPath
				}
			} else {
				// if meeting ".." means cut path
				currentPath = cutPath(currentPath)
				fmt.Println("..", currentPath)
			}
		} else {

			lsCommand := strings.Split(command, "\n")

			fmt.Println("ls command ", lsCommand, len(lsCommand))

			for i := 1; i <= len(lsCommand)-1; i++ {

				outPutPre := lsCommand[i]
				fmt.Println("file lsCommand", lsCommand[i])
				if outPutPre == "" {
					break
				}

				//
				outPut := strings.Split(outPutPre, " ")

				isDir := strings.Compare(outPut[0], "dir")

				if isDir == 0 {
					p := currentPath + "/" + outPut[1]
					data := file{
						position: i,
						size:     0,
						name:     outPut[1],
						isfile:   false,
						path:     p,
					}
					fmt.Println(pPath, outPut[1])
					b = b.insert(pPath, data)
					addSize(b, pPath, data)
				} else {
					p := currentPath + "/" + outPut[1]
					fmt.Println(pPath, outPut[1])

					size, err := strconv.Atoi(outPut[0])
					if err != nil {
						return 0, err
					}

					name := outPut[1]

					data := file{
						position: i,
						size:     size,
						name:     name,
						isfile:   true,
						path:     p,
					}

					fmt.Println("pPath", pPath, data.name, data.size)
					b = b.insert(pPath, data)
					addSize(b, pPath, data)
				}
			}
		}
	}

	sum := b.root.atMostSum()
	return sum, nil

}

func atLeastSize(fileData string) (int, error) {
	input := strings.TrimSpace(fileData)
	inputTrim := strings.TrimSpace(input)
	commands := strings.Split(inputTrim, "$")
	var b = &BinaryTree{
		root: nil,
	}
	pPath := ""
	currentPath := ""
	for _, command := range commands {
		isCd := strings.Contains(command, " cd ")
		if isCd == true {
			commandTrim := strings.TrimSpace(command)

			if strings.Compare(commandTrim[3:], "..") != 0 {

				if strings.Compare(commandTrim[3:], "/") == 0 {

					data := file{
						position: 0,
						size:     0,
						name:     "/",
						isfile:   false,
						path:     "/",
					}
					b = b.insert("/", data)
					pPath = "/"
				} else {
					// pick current parent
					cdCommand := strings.Split(commandTrim, " ")
					parent := cdCommand[1]
					currentPath = currentPath + "/" + parent
					pPath = currentPath
				}
			} else {
				// if meeting ".." means cut path
				currentPath = cutPath(currentPath)
				fmt.Println("..", currentPath)
			}
		} else {

			lsCommand := strings.Split(command, "\n")

			fmt.Println("ls command ", lsCommand, len(lsCommand))

			for i := 1; i <= len(lsCommand)-1; i++ {

				outPutPre := lsCommand[i]
				fmt.Println("file lsCommand", lsCommand[i])
				if outPutPre == "" {
					break
				}

				//
				outPut := strings.Split(outPutPre, " ")

				isDir := strings.Compare(outPut[0], "dir")

				if isDir == 0 {
					p := currentPath + "/" + outPut[1]
					data := file{
						position: i,
						size:     0,
						name:     outPut[1],
						isfile:   false,
						path:     p,
					}
					fmt.Println(pPath, outPut[1])
					b = b.insert(pPath, data)
					addSize(b, pPath, data)
				} else {
					p := currentPath + "/" + outPut[1]
					fmt.Println(pPath, outPut[1])

					size, err := strconv.Atoi(outPut[0])
					if err != nil {
						return 0, err
					}

					name := outPut[1]

					data := file{
						position: i,
						size:     size,
						name:     name,
						isfile:   true,
						path:     p,
					}

					fmt.Println("pPath", pPath, data.name, data.size)
					b = b.insert(pPath, data)
					addSize(b, pPath, data)
				}
			}
		}
	}

	// get the least space
	total := 70000000
	uesdSpace := b.root.data.size
	upNeedSpace := 30000000
	unUsed := total - uesdSpace
	if unUsed < upNeedSpace {
		fmt.Println("need delete directory")
		atLessSize := upNeedSpace - unUsed
		// delete least directory
		m := b.root.atLeast(atLessSize)
		return m, nil

	} else {

		return 0, nil
	}

}
