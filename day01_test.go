package main

import (
	"testing"

	"github.com/lithammer/dedent"
)

func TestMaxCalorie(t *testing.T) {
	testInput := string(`
		1000
		2000
		3000

		4000

		5000
		6000

		7000
		8000
		9000

		10000
	`)

	input := dedent.Dedent(testInput)
	want := 24000
	t.Run(input, func(t *testing.T) {
		r, err := MaxCalorie(input)
		if err != nil {
			t.Error(err)
		}

		if r != want {
			t.Errorf("got %d, want %d", r, want)
		}
	})
}

func TestTopThreeCalorie(t *testing.T) {
	testInput := string(`
		1000
		2000
		3000

		4000

		5000
		6000

		7000
		8000
		9000

		10000
	`)

	input := dedent.Dedent(testInput)
	want := 45000
	t.Run(input, func(t *testing.T) {
		r, err := TopThreeCalorie(input)
		if err != nil {
			t.Error(err)
		}

		if r != want {
			t.Errorf("got %d, want %d", r, want)
		}
	})
}
