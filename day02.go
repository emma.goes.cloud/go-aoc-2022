package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

var shapeMap = map[string]int{
	"Rock":     1,
	"Paper":    2,
	"Scissors": 3,
}

func mainDay02() {
	data, err := os.ReadFile("data/day02.txt")
	if err != nil {
		log.Fatal(err)
	}

	totalScor := totalScore(string(data))
	fmt.Println(totalScor)
	totalScorEnd := totalScoreByEnd(string(data))
	fmt.Println(totalScorEnd)

}

func totalScore(input string) int {

	dataTrim := strings.TrimSpace(input)
	rounds := strings.Split(dataTrim, "\n")

	var totalScore = 0
	for _, round := range rounds {
		chooses := strings.Fields(round)

		oppoChoose := chooses[0]
		myChoose := chooses[1]

		opShape := chooseShape(oppoChoose)
		myShape := chooseShape(myChoose)

		myScore := myScor(opShape, myShape)

		totalScore = totalScore + myScore
	}

	return totalScore

}

func chooseShape(choose string) string {
	var shape = ""
	switch choose {
	case "A", "X":
		shape = "Rock"
	case "B", "Y":
		shape = "Paper"
	case "C", "Z":
		shape = "Scissors"
	}
	return shape
}

func myScor(opShape string, myShape string) int {

	var myScore = 0
	switch opShape {
	case "Rock":
		switch myShape {
		case "Rock":
			myScore = 3 + 1
		case "Paper":
			myScore = 6 + 2
		case "Scissors":
			myScore = 0 + 3
		}
	case "Paper":
		switch myShape {
		case "Rock":
			myScore = 0 + 1
		case "Paper":
			myScore = 3 + 2
		case "Scissors":
			myScore = 6 + 3
		}

	case "Scissors":
		switch myShape {
		case "Rock":
			myScore = 6 + 1
		case "Paper":
			myScore = 0 + 2
		case "Scissors":
			myScore = 3 + 3
		}

	}

	return myScore

}

func scoreByEnd(oppoChoose string, end string) int {
	var myScore = 0
	switch oppoChoose {
	case "A":
		switch end {
		case "X":
			myScore = 0 + 3
		case "Y":
			myScore = 3 + 1
		case "Z":
			myScore = 6 + 2

		}
	case "B":
		switch end {
		case "X":
			myScore = 0 + 1
		case "Y":
			myScore = 3 + 2
		case "Z":
			myScore = 6 + 3

		}

	case "C":
		switch end {
		case "X":
			myScore = 0 + 2
		case "Y":
			myScore = 3 + 3
		case "Z":
			myScore = 6 + 1

		}

	}
	return myScore
}

func totalScoreByEnd(input string) int {

	dataTrim := strings.TrimSpace(input)
	rounds := strings.Split(dataTrim, "\n")

	var totalScore = 0
	for _, round := range rounds {
		chooses := strings.Fields(round)

		oppoChoose := chooses[0]
		myChoose := chooses[1]

		myScore := scoreByEnd(oppoChoose, myChoose)

		totalScore = totalScore + myScore
	}
	return totalScore

}
