package main

import (
	"log"
	"os"
	"testing"
)

func TestTotalPosition(t *testing.T) {
	testInput, err := os.ReadFile("data/day09_test.txt")
	if err != nil {
		log.Fatal(err)
	}

	input := string(testInput)
	want := 13

	t.Run(input, func(t *testing.T) {
		got, err := totalPosition(input)
		if err != nil {
			t.Error(err)
		}

		if got != want {
			t.Errorf("got %d, want %d", got, want)
		}
	})
}
